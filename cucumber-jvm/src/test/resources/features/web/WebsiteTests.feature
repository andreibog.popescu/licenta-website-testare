@all @website
Feature: Website

  Scenario: Register and Login - Success
    Given I register a new user
    When I login with the current user
    Then the correct user profile is displayed

  Scenario Outline: Login - Failure - HTML5 validation
    When I try to login with <email> and <password>
    Then HTML login validation fails
    Examples:
      | email      | password |
      | admin      | licenta  |
      | admin@     | licenta  |
      | admin@fmi. | licenta  |

  Scenario Outline: Login - Failure - API Errors
    When I try to login with <email> and <password>
    Then login validation fails with '<error>' message
    Examples:
      | email             | password | error                      |
      | admin@test        | licenta  | Invalid email or password. |
      | admin@licenta.fmi | licenta1 | Invalid email or password. |

  Scenario Outline: Register - Failure - Email
    Given a new user is generated randomly
    When I try to register a new user with email: <email>
    Then email registration fails with '<error>' message
    Examples:
      | email              | error                                      |
      |                    | Invalid Email.                             |
      | admin              | HTML                                       |
      | admin@             | HTML                                       |
      | admin@fmi.         | HTML                                       |
      | admin@5342.licenta | Invalid Email.                             |
      | admin@fmi.licenta. | HTML                                       |
      | admin@licenta.fmi  | Email has been registered to another user. |

  Scenario Outline: Register - Failure - Username
    Given a new user is generated randomly
    When I try to register a new user with username: <username>
    Then username registration fails with '<error>' message
    Examples:
      | username | error                |
      |          | Invalid username.    |
      | admin    | User already exists. |

  Scenario Outline: Register - Failure - Password
    Given a new user is generated randomly
    When I try to register a new user with password <password> and matching password <matchingPassword>
    Then password registration fails with '<error>' message
    Examples:
      | password                | matchingPassword        | error                                                                 |
      |                         |                         | Password or matching password cannot be empty.                        |
      | admin                   | admin1                  | Retyped password does not match provided password.                    |
      | thisisaverylongpassword | thisisaverylongpassword | Password should be less than 20 characters in length.                 |
      | short                   | short                   | Password should be more than 8 characters in length.                  |
      | Test                    | Test                    | Password should not be the same or contain the username.              |
      | 111                     | 111                     | Password should contain at least one upper case alphabetic character. |
      | 111                     | 111                     | Password should contain at least one lower case alphabetic character. |
      | 111                     | 111                     | Password should contain at least one special character.               |
      | aaaaA!                  | aaaaA!                  | Password should contain at least one numeric character.               |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one upper case alphabetic character. |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one lower case alphabetic character. |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one numeric character.               |

  Scenario: Profile - Can edit own description
    Given I register a new user
    When I try to edit the user description with 'Test Description'
    Then the current user description has been edited successfully with 'Test Description'

  Scenario: Profile - Cannot edit other users description
    Given I register 2 different users
    And I login with user 1
    And I go to the user 2 profile
    When I try to edit the user description with 'Test Description'
    Then I cannot edit other users description

  Scenario: Tweets - Post new tweet
    Given I register a new user
    When I post a tweet with content 'Test Tweet'
    Then a tweet has been posted successfully with content 'Test Tweet'

  Scenario: Tweets - View other people tweets
    Given I register 2 different users
    And I login with user 2
    When I post a tweet with content 'Test Tweet'
    And I login with user 1
    And I go to the user 2 profile
    Then a tweet for user 2 has been posted successfully with content 'Test Tweet'

  Scenario: Tweets - Like own tweet
    Given I register a new user
    When I post a tweet with content 'Test Tweet'
    And I like tweets with content 'Test Tweet'
    Then the tweets with content 'Test Tweet' have been successfully liked

  Scenario: Tweets - Like other user tweet
    Given I register 2 different users
    And I login with user 2
    And I post a tweet with content 'Test Tweet'
    And I login with user 1
    And I go to the user 2 profile
    When I like tweets with content 'Test Tweet'
    Then the tweets with content 'Test Tweet' have been successfully liked

  Scenario: Tweets - Unlike own tweet
    Given I register a new user
    When I post a tweet with content 'Test Tweet'
    And I like tweets with content 'Test Tweet'
    And I unlike tweets with content 'Test Tweet'
    Then the tweets with content 'Test Tweet' have been successfully unliked

  Scenario: Tweets - Like other user tweet
    Given I register 2 different users
    And I login with user 2
    And I post a tweet with content 'Test Tweet'
    And I login with user 1
    And I go to the user 2 profile
    When I like tweets with content 'Test Tweet'
    Then the tweets with content 'Test Tweet' have been successfully liked

  Scenario: Tweets - Multiple likes
    Given I register 3 different users
    And I login with user 3
    And I post a tweet with content 'Test Tweet'
    When I login with user 1
    And I go to the user 3 profile
    And I like tweets with content 'Test Tweet'
    And I login with user 2
    And I go to the user 3 profile
    And I like tweets with content 'Test Tweet'
    Then the tweets with content 'Test Tweet' have been successfully liked

  Scenario: Followers - Follow user
    Given I register 2 different users
    And I login with user 1
    When I follow user 2
    Then the user has been successfully followed

  Scenario: Followers - Unfollow user
    Given I register 2 different users
    And I login with user 1
    When I follow user 2
    And I unfollow user 2
    Then the user has been successfully unfollowed

  Scenario: Followers - Following user tweets on profile
    Given I register 2 different users
    And I login with user 2
    And I post a tweet with content 'Test Tweet'
    And I login with user 1
    When I follow user 2
    And I go to the user 1 profile
    Then tweets from user 2 show up on profile

  Scenario: Search - Users and Tweets
    Given I register a new user
    And I post a tweet with content 'Test Tweet'
    When I search for 'Test'
    Then search results are present for value 'Test'





