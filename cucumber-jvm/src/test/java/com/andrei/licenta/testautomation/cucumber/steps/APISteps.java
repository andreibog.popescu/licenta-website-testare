package com.andrei.licenta.testautomation.cucumber.steps;

import com.andrei.licenta.testautomation.model.*;
import com.andrei.licenta.testautomation.utils.JSONUtils;
import com.andrei.licenta.testautomation.utils.LogProvider;
import com.fasterxml.jackson.core.type.TypeReference;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Fail;
import org.assertj.core.api.SoftAssertions;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class APISteps implements LogProvider {

    private SharedDataAndActions sharedDataAndActions;

    private SoftAssertions softAssertions = new SoftAssertions();

    public APISteps(SharedDataAndActions sharedDataAndActions) {
        this.sharedDataAndActions = sharedDataAndActions;
    }

    @Given("^I register a new user through API$")
    public void iRegisterANewUserThroughAPI() throws Throwable {
        sharedDataAndActions.generateNewUser();
        sharedDataAndActions.getUserRequestHandler().registerUser(sharedDataAndActions.getCurrentUser());

        iAuthenticateWithAPIUsingTheCurrentUser();
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getUserDetails(sharedDataAndActions.getCurrentUser());
        UserModel responseUser = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        sharedDataAndActions
                .getUsers()
                .stream()
                .filter(userModel -> Objects.equals(userModel.getUsername(), responseUser.getUsername()))
                .findAny()
                .ifPresent(userModel -> {
                    userModel.setId(responseUser.getId());
                    sharedDataAndActions.getCurrentUser().setId(responseUser.getId());
                });

        resetAuth();
    }

    @Then("^the current user has been registered successfully through API$")
    public void theCurrentUserHasBeenRegisteredSuccessfullyThroughAPI() throws Throwable {
        assertThat(
                sharedDataAndActions
                        .getUserRequestHandler()
                        .getApiConnectionResponse()
                        .getResult()
                        .getCode()
        ).isEqualTo(ActionResultCode.OK);
    }

    @When("^I try to register a new user with email using API: (.*)$")
    public void iTryToRegisterANewUserWithEmailUsingAPI(String email) throws Throwable {
        sharedDataAndActions.getCurrentUser().setEmail(email);
        sharedDataAndActions.getUserRequestHandler().registerUser(sharedDataAndActions.getCurrentUser());
    }

    @When("^I try to register a new user with username using API: (.*)$")
    public void iTryToRegisterANewUserWithUsernameUsingAPI(String username) throws Throwable {
        sharedDataAndActions.getCurrentUser().setUsername(username);
        sharedDataAndActions.getUserRequestHandler().registerUser(sharedDataAndActions.getCurrentUser());
    }

    @When("^I try to register a new user using API using password (.*) and matching password (.*)$")
    public void iTryToRegisterANewUserUsingAPIUsingPasswordAndMatchingPassword(String password, String matchingPassword) throws Throwable {
        sharedDataAndActions.getCurrentUser().setPassword(password);
        sharedDataAndActions.getCurrentUser().setMatchingPassword(matchingPassword);
        sharedDataAndActions.getUserRequestHandler().registerUser(sharedDataAndActions.getCurrentUser());
    }

    @Then("^(.*) registration with api fails with '(.*)' message$")
    public void typeRegistrationWithApiFailsWithErrorMessage(String type, String error) throws Throwable {

        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getApiConnectionResponse();

        softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_CREATION_FAIL);
        List<ActionResult> errorDetails = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<ActionResult>>() {
                }
        );

        softAssertions.assertThat(errorDetails.stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), error))).isTrue();

        switch (type.toLowerCase()) {
            case "email":
                softAssertions.assertThat(errorDetails.stream().anyMatch(actionResult -> Objects.equals(actionResult.getCode(), ActionResultCode.EMAIL_ERROR))).isTrue();
                break;
            case "password":
                softAssertions.assertThat(errorDetails.stream().anyMatch(actionResult -> Objects.equals(actionResult.getCode(), ActionResultCode.PASSWORD_ERROR))).isTrue();
                break;
            case "username":
                softAssertions.assertThat(errorDetails.stream().anyMatch(actionResult -> Objects.equals(actionResult.getCode(), ActionResultCode.USERNAME_ERROR))).isTrue();
                break;
            default:
                softAssertions.fail("Incorrect registration type {}", type);
        }

        softAssertions.assertAll();
    }

    @And("^I authenticate with API using the current user$")
    public void iAuthenticateWithAPIUsingTheCurrentUser() throws Throwable {
        AuthenticationModel authenticationModel = new AuthenticationModel(
                sharedDataAndActions.getCurrentUser().getEmail(),
                sharedDataAndActions.getCurrentUser().getPassword()
        );
        sharedDataAndActions.getUserRequestHandler().setAuthenticationModel(authenticationModel);
        sharedDataAndActions.getTweetRequestHandler().setAuthenticationModel(authenticationModel);
        sharedDataAndActions.getSearchRequestHandler().setAuthenticationModel(authenticationModel);
    }

    private void resetAuth(){
        sharedDataAndActions.getUserRequestHandler().setAuthenticationModel(null);
        sharedDataAndActions.getTweetRequestHandler().setAuthenticationModel(null);
        sharedDataAndActions.getSearchRequestHandler().setAuthenticationModel(null);
    }

    @And("^I authenticate with API using user (\\d+)$")
    public void iAuthenticateWithAPIUsingUser(int id) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);

        iAuthenticateWithAPIUsingTheCurrentUser();
    }

    @Given("^I register through API (\\d+) different users$")
    public void iRegisterThroughAPIDifferentUsers(int count) throws Throwable {
        for (int pos = 0; pos < count; pos++) {
            iRegisterANewUserThroughAPI();
        }
    }

    @When("^I try to edit using API the user description with '(.*)'$")
    public void iTryToEditUsingAPITheUserDescriptionWith(String description) throws Throwable {
        sharedDataAndActions.getCurrentUser().setDescription(description);
        sharedDataAndActions.getUserRequestHandler().editDescription(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @Then("^the current user description has been edited using API successfully with '(.*)'$")
    public void theCurrentUserDescriptionHasBeenEditedUsingAPISuccessfullyWith(String description) throws Throwable {
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getUserDetails(
                sharedDataAndActions.getCurrentUser()
        );

        UserModel userDetails = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        assertThat(userDetails.getDescription()).isEqualTo(description);
    }

    @When("^I try to edit using API the user description for user (\\d+) with '(.*)'$")
    public void iTryToEditUsingAPITheUserDescriptionForUserWithTestDescription(int id, String description) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);

        iTryToEditUsingAPITheUserDescriptionWith(description);
    }

    @Then("^I cannot edit through API other users description$")
    public void iCannotEditThroughAPIOtherUsersDescription() throws Throwable {
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getApiConnectionResponse();

        softAssertions.assertThat(response.getResult().getReason()).isEqualTo("This user cannot edit details for another user.");
        softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.ERROR);
        softAssertions.assertAll();
    }

    @When("^I (unfollow|follow) using API user (\\d+)$")
    public void iFollowUsingAPIUser(String status, int id) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);

        if (Objects.equals("follow", status)) {
            sharedDataAndActions.getUserRequestHandler().followUser(sharedDataAndActions.getCurrentUser());
        } else {
            sharedDataAndActions.getUserRequestHandler().unfollowUser(sharedDataAndActions.getCurrentUser());
        }
    }

    @Then("^user (\\d+) is (not |)following user (\\d+)$")
    public void userIsFollowingUser(int id1, String status, int id2) throws Throwable {
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getApiConnectionResponse();

        sharedDataAndActions.setCurrentUserBy(id1);
        sharedDataAndActions.setCurrentUserBy(id2);
        List<FollowerModel> followers = JSONUtils.getJsonMapper().convertValue(
                sharedDataAndActions.getUserRequestHandler().getFollowers(
                        sharedDataAndActions.getCurrentUser()
                ).getDetails(),
                new TypeReference<List<FollowerModel>>() {
                }
        );

        if (Objects.equals(status.trim(), "not")) {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_UNFOLLOW_SUCCESS);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("User unfollow successful.");
            softAssertions.assertAll();

            logger().info("Check if user {} is not in follower list", sharedDataAndActions.getUsers().get(id1 - 1).getUsername());

            Optional<FollowerModel> actualFollowerOptional = followers
                    .stream()
                    .filter(followerModel -> followerModel.getUser().getUsername().equalsIgnoreCase(sharedDataAndActions.getUsers().get(id1 - 1).getUsername())).findFirst();

            if (actualFollowerOptional.isPresent() && actualFollowerOptional.get().isActive()) {
                Fail.fail("User " + sharedDataAndActions.getUsers().get(id2 - 1).getUsername() + " not followed by " + sharedDataAndActions.getUsers().get(id1 - 1).getUsername());
            }
        } else {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_FOLLOW_SUCCESS);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("User follow successful.");
            softAssertions.assertAll();

            logger().info("Check if user {} is in follower list", sharedDataAndActions.getUsers().get(id1 - 1).getUsername());

            Optional<FollowerModel> actualFollowerOptional = followers
                    .stream()
                    .filter(followerModel -> followerModel.getUser().getUsername().equalsIgnoreCase(sharedDataAndActions.getUsers().get(id1 - 1).getUsername()))
                    .findFirst();

            if (!actualFollowerOptional.isPresent()) {
                Fail.fail("User " + sharedDataAndActions.getUsers().get(id2 - 1).getUsername() + " not followed by " + sharedDataAndActions.getUsers().get(id1 - 1).getUsername());
            }
        }
    }

    @Then("^an error response is returned for duplicate (unfollow|follow)$")
    public void anErrorResponseIsReturnedForDuplicateFollow(String status) throws Throwable {
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getApiConnectionResponse();

        if (Objects.equals(status, "follow")) {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_FOLLOW_DUPLICATE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("User already followed.");
        } else {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_UNFOLLOW_DUPLICATE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("User already unfollowed.");
        }

        softAssertions.assertAll();
    }

    @Then("^an error response is returned for unfollow failure$")
    public void anErrorResponseIsReturnedForUnfollowFailure() throws Throwable {
        GenericResponse response = sharedDataAndActions.getUserRequestHandler().getApiConnectionResponse();

        softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.USER_UNFOLLOW_FAILURE);
        softAssertions.assertThat(response.getResult().getReason()).isEqualTo("User must be followed first.");

        softAssertions.assertAll();
    }

    @When("^I post a tweet using API with content '(.*)'$")
    public void iPostATweetUsingAPIWithContent(String tweetContent) throws Throwable {
        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent(tweetContent);
        tweetModel.setUsername(sharedDataAndActions.getCurrentUser().getUsername());
        sharedDataAndActions.getTweetRequestHandler().addTweet(tweetModel);
        }

    @Then("^a tweet has (not |)been posted using API successfully with content '(.*)'$")
    public void aTweetHasBeenPostedUsingAPISuccessfullyWithContent(String status, String tweetContent) throws Throwable {
        GenericResponse response = sharedDataAndActions.getTweetRequestHandler().getTweets(
                sharedDataAndActions.getCurrentUser()
        );

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase(tweetContent))
                .findFirst();

        if (Objects.equals(status.trim(), "not")) {
            logger().info("Checking new tweet has not been successfully posted");
            actualTweetContent.ifPresent(tweetModel -> Fail.fail("Tweet has been posted"));
        } else {
            logger().info("Checking new tweet is successfully posted");
            if (!actualTweetContent.isPresent()) {
                Fail.fail("No tweet has been posted");
            } else {
                softAssertions.assertThat(actualTweetContent.get().getUser().getUsername()).isEqualTo(sharedDataAndActions.getCurrentUser().getUsername());
                softAssertions.assertThat(actualTweetContent.get().getLikeCount()).isZero();
            }
        }

        softAssertions.assertAll();
    }

    @When("^I post a tweet using API for user (\\d+) with content '(.*)'$")
    public void iPostATweetUsingAPIForUserWithContentTestTweet(int id, String tweetContent) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);
        iPostATweetUsingAPIWithContent(tweetContent);
    }

    @Then("^a tweet has (not |)been posted using API successfully for user (\\d+) with content '(.*)'$")
    public void aTweetHasNotBeenPostedUsingAPISuccessfullyForUserWithContentTestTweet(String status, int id, String tweetContent) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);

        aTweetHasBeenPostedUsingAPISuccessfullyWithContent(status, tweetContent);
    }

    @When("^I (unlike|like) a tweet using API with content '(.*)'$")
    public void iLikeATweetUsingAPIWithContentTestTweet(String status, String tweetContent) throws Throwable {
        GenericResponse response = sharedDataAndActions.getTweetRequestHandler().getTweets(
                sharedDataAndActions.getCurrentUser()
        );

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase(tweetContent))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Fail.fail("No tweet has been posted");
        } else {
            if (Objects.equals(status, "like")) {
                sharedDataAndActions.getTweetRequestHandler().likeTweet(actualTweetContent.get());
            } else {
                sharedDataAndActions.getTweetRequestHandler().unlikeTweet(actualTweetContent.get());
            }
        }
    }

    @Then("^the tweet has been (unliked|liked) using API successfully$")
    public void aTweetHasBeenLikedUsingAPISuccessfullyWithContentTestTweet(String status) throws Throwable {
        GenericResponse response = sharedDataAndActions.getTweetRequestHandler().getApiConnectionResponse();

        if (Objects.equals(status, "liked")) {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_LIKE_SUCCESS);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet like successful.");
        } else {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_UNLIKE_SUCCESS);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet unlike successful.");
        }
        softAssertions.assertAll();
    }

    @Then("^an error response is returned for duplicate (like|unlike)$")
    public void anErrorResponseIsReturnedForDuplicateLike(String status) throws Throwable {
        GenericResponse response = sharedDataAndActions.getTweetRequestHandler().getApiConnectionResponse();

        if (Objects.equals(status, "like")) {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_LIKE_DUPLICATE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet already liked.");
        } else {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_UNLIKE_DUPLICATE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet already unlike.");
        }

        softAssertions.assertAll();
    }

    @When("^I like a tweet using API that does not exist$")
    public void iLikeATweetUsingAPIThatDoesNotExist() throws Throwable {
        TweetModel tweetModel = new TweetModel();
        tweetModel.setId("999999999999");

        sharedDataAndActions.getTweetRequestHandler().likeTweet(tweetModel);
    }

    @Then("^an error response is returned for (unlike|like) failure$")
    public void anErrorResponseIsReturnedForLikeFailure(String status) throws Throwable {
        GenericResponse response = sharedDataAndActions.getTweetRequestHandler().getApiConnectionResponse();

        if (Objects.equals(status, "like")) {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_LIKE_FAILURE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet does not exist.");
        } else {
            softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.TWEET_UNLIKE_FAILURE);
            softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Tweet needs to be liked first.");
        }

        softAssertions.assertAll();
    }

    @When("^I search users using API for '(.*)'$")
    public void iSearchUsersUsingAPIForTest(String searchValue) throws Throwable {
        sharedDataAndActions.getSearchRequestHandler().searchUsers(searchValue);
    }

    @When("^I search tweets using API for '(.*)'$")
    public void iSearchTweetsUsingAPIForTest(String searchValue) throws Throwable {
        sharedDataAndActions.getSearchRequestHandler().searchTweets(searchValue);
    }

    @Then("^search results have (not |)been retrieved from API$")
    public void searchResultsHaveBeenRetrievedFromAPI(String status) throws Throwable {
        GenericResponse response = sharedDataAndActions.getSearchRequestHandler().getApiConnectionResponse();

        List<UserSearchModel> searchResults = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<UserSearchModel>>() {
                }
        );

        softAssertions.assertThat(response.getResult().getCode()).isEqualTo(ActionResultCode.OK);
        softAssertions.assertThat(response.getResult().getReason()).isEqualTo("Search result retrieved.");

        if (Objects.equals(status.trim(), "not")) {
            softAssertions.assertThat(searchResults).isEmpty();
        } else {
            softAssertions.assertThat(searchResults).isNotEmpty();
        }

        softAssertions.assertAll();
    }
}
