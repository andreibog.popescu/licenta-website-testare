package com.andrei.licenta.testautomation.junit.web.entry;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import com.andrei.licenta.testautomation.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RegisterTest extends TestBase {

    @Test void registerUser() {
        UserModel user = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user);

        Assertions.assertTrue(profilePage.isCorrectProfile(user.getUsername()));
    }

    @Test void registerUserErrorUsername() {
        loginPage.goToRegisterPage();
        UserModel user = UserModel.generateRandomUser();

        Assertions.assertAll(
                () -> {
                    user.setUsername("");
                    registerPage.doRegister(user);
                    Assertions.assertEquals("Invalid username.", registerPage.getRegisterUsernameErrorMessage());
                },
                () -> {
                    user.setUsername("admin");
                    registerPage.doRegister(user);
                    Assertions.assertEquals("User already exists.", registerPage.getRegisterUsernameErrorMessage());
                }
        );
    }

    @Test void registerUserErrorEmail() {
        loginPage.goToRegisterPage();
        UserModel user = UserModel.generateRandomUser();

        Assertions.assertAll(
                () -> {
                    user.setEmail("");
                    registerPage.doRegister(user);
                    Assertions.assertEquals("Invalid Email.", registerPage.getRegisterEmailErrorMessage());
                },
                () -> {
                    user.setEmail("admin");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.isEmailValidationTriggered());
                },
                () -> {
                    user.setEmail("admin@");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.isEmailValidationTriggered());
                },
                () -> {
                    user.setEmail("admin@fmi.");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.isEmailValidationTriggered());
                },
                () -> {
                    user.setEmail("admin@532423.licenta");
                    registerPage.doRegister(user);
                    Assertions.assertEquals("Invalid Email.", registerPage.getRegisterEmailErrorMessage());
                },
                () -> {
                    user.setEmail("admin@fmi.licenta.");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.isEmailValidationTriggered());
                },
                () -> {
                    user.setEmail("admin@licenta.fmi");
                    registerPage.doRegister(user);
                    Assertions.assertEquals("Email has been registered to another user.", registerPage.getRegisterEmailErrorMessage());
                }
        );
    }

    @Test void registerUserErrorPassword() {
        loginPage.goToRegisterPage();
        UserModel user = UserModel.generateRandomUser();

        Assertions.assertAll(
                () -> {
                    user.setPassword("");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password or matching password cannot be empty."));
                },
                () -> {
                    user.setPassword("admin");
                    user.setMatchingPassword("admin1");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Retyped password does not match provided password."));
                },
                () -> {
                    user.setPassword("thisisaverylongpassword");
                    user.setMatchingPassword("thisisaverylongpassword");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should be less than 20 characters in length."));
                },
                () -> {
                    user.setPassword("short");
                    user.setMatchingPassword("short");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should be more than 8 characters in length."));
                },
                () -> {
                    user.setPassword("Test");
                    user.setPassword("Test");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should not be the same or contain the username."));
                },
                () -> {
                    user.setPassword("1111");
                    user.setMatchingPassword("1111");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one upper case alphabetic character."));
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one lower case alphabetic character."));
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one special character."));
                },
                () -> {
                    user.setPassword("aaaaA!");
                    user.setMatchingPassword("aaaaA!");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one numeric character."));
                },
                () -> {
                    user.setPassword("!@$#@%$%$#!");
                    user.setMatchingPassword("!@$#@%$%$#!");
                    registerPage.doRegister(user);
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one upper case alphabetic character."));
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one lower case alphabetic character."));
                    Assertions.assertTrue(registerPage.getRegisterPasswordErrorMessage().contains("Password should contain at least one numeric character."));
                }
        );
    }

}
