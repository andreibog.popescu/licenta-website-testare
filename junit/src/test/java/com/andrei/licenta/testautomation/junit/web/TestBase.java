package com.andrei.licenta.testautomation.junit.web;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.handlers.UserRequestHandler;
import com.andrei.licenta.testautomation.junit.TestExtension;
import com.andrei.licenta.testautomation.utils.LogProvider;
import com.andrei.licenta.testautomation.webdriver.WebDriverFactory;
import com.andrei.licenta.testautomation.webdriver.pageobjects.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@ExtendWith(TestExtension.class)
public class TestBase implements LogProvider {

    private static WebDriverFactory webDriverFactory = new WebDriverFactory();

    private static APIConnection apiConnection = new APIConnection();

    protected static UserRequestHandler userRequestHandler;

    @BeforeAll
    public static void setup() {
        webDriverFactory.getDriver("chrome", WebDriverFactory.Browser.CHROME);

        apiConnection.setBaseURI("http://localhost");
        apiConnection.setPort(8080);

        userRequestHandler = new UserRequestHandler(apiConnection);
    }

    protected Login loginPage;
    protected Register registerPage;
    protected Profile profilePage;
    protected Search searchPage;
    protected Header mainHeader;

    @BeforeEach
    public void init(TestInfo testInfo) {
        logger().info("Starting test: [{}]", testInfo.getDisplayName());
        WebDriver webDriver = webDriverFactory.getDriver("chrome");

        initPageObjects(webDriver);

        webDriver.get("http://localhost:8080");
    }

    @AfterEach
    public void cleanup() {
        logger().info("Logging out");
        mainHeader.logout();

        userRequestHandler.cleanup();
        userRequestHandler.setAuthenticationModel(null);
        apiConnection.resetHeaders();
    }

    private void initPageObjects(WebDriver webDriver){
        loginPage = new Login(webDriver);
        registerPage = new Register(webDriver);
        profilePage = new Profile(webDriver);
        searchPage = new Search(webDriver);
        mainHeader = new Header(webDriver);
    }
}
