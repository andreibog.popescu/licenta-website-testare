package com.andrei.licenta.testautomation.junit.web.search;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import com.andrei.licenta.testautomation.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SearchTest extends TestBase {

    @Test void searchBothTweetsAndUsers(){
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        mainHeader.doSearch("Test");

        Assertions.assertTrue(searchPage.isDisplayed());

        Assertions.assertAll(
                () -> Assertions.assertEquals("Users found", searchPage.getSearchUsersText()),
                () -> Assertions.assertEquals("Tweets found", searchPage.getSearchTweetsText()),
                () -> Assertions.assertTrue(searchPage.getAllSearchUsers().size() != 0),
                () -> Assertions.assertTrue(searchPage.getAllSearchTweets().size() != 0)
        );
    }

    @Test void noSearchResults(){
        loginPage.doLogin("admin@licenta.fmi", "licenta");

        mainHeader.doSearch("z");

        Assertions.assertTrue(searchPage.isDisplayed());

        Assertions.assertAll(
                () -> Assertions.assertEquals("No users found", searchPage.getSearchUsersText()),
                () -> Assertions.assertEquals("No tweets found", searchPage.getSearchTweetsText()),
                () -> Assertions.assertEquals(0, searchPage.getAllSearchUsers().size()),
                () -> Assertions.assertEquals(0, searchPage.getAllSearchTweets().size())
        );
    }

}
