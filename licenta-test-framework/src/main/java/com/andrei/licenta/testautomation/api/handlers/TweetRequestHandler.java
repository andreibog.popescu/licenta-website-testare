package com.andrei.licenta.testautomation.api.handlers;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.APIPaths;
import com.andrei.licenta.testautomation.model.GenericResponse;
import com.andrei.licenta.testautomation.model.TweetModel;
import com.andrei.licenta.testautomation.model.UserModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;

public class TweetRequestHandler extends RequestHandler {
    public TweetRequestHandler(APIConnection apiConnection) {
        super(apiConnection);
    }

    public GenericResponse addTweet(TweetModel tweetContent) throws JsonProcessingException {
        logger().info("Adding new tweet {} for user {}", tweetContent.getContent(), tweetContent.getUsername());
        getApiConnection()
                .setBody(getJson(tweetContent), ContentType.JSON)
                .setBasePath(String.format(APIPaths.tweetUserName, tweetContent.getUsername()))
                .sendPost();

        return getApiConnectionResponse();
    }

    public GenericResponse getTweets(UserModel user) {
        logger().info("Getting tweets for user {}", user.getUsername());
        getApiConnection()
                .setBasePath(String.format(APIPaths.tweetUserName, user.getUsername()))
                .sendGet();

        return getApiConnectionResponse();
    }

    public GenericResponse likeTweet(TweetModel tweetModel) {
        logger().info("Liking tweet with id {}", tweetModel.getId());
        getApiConnection()
                .setBasePath(String.format(APIPaths.likeTweet, tweetModel.getId()))
                .sendPost();

        return getApiConnectionResponse();
    }

    public GenericResponse unlikeTweet(TweetModel tweetModel) {
        logger().info("Unliking tweet with id {}", tweetModel.getId());
        getApiConnection()
                .setBasePath(String.format(APIPaths.unlikeTweet, tweetModel.getId()))
                .sendPost();

        return getApiConnectionResponse();
    }
}
