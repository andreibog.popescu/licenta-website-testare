package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.model.TweetSearchModel;
import com.andrei.licenta.testautomation.model.UserSearchModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class Search extends PageObjectBase implements LogProvider {

    @FindBy(xpath = "//*[@id=\"userSearch\"]/div[1]/b/span")
    private WebElement searchHeader;
    @FindAll({
            @FindBy(xpath = "//*[@id=\"userSearch\"]/div[2]/b/span"),
            @FindBy(xpath = "//*[@id=\"noSearchResultsUsers\"]")
    })
    private WebElement userSearchResult;
    @FindAll({
            @FindBy(xpath = "//*[@id=\"tweetSearch\"]/div[1]/b/span"),
            @FindBy(xpath = "//*[@id=\"noSearchResultsTweets\"]")
    })
    private WebElement tweetSearchResult;
    @FindBy(xpath = "//*[@id=\"users\"]/li")
    private List<WebElement> usersSearchElements;
    @FindBy(xpath = "//*[@id=\"tweets\"]/li")
    private List<WebElement> tweetsSearchElements;

    public Search(WebDriver webDriver) {
        super(webDriver);
    }

    private String tweetLocator = "//li[starts-with(@id,'tweet')]";
    private String tweetUsernameLocator = "//span[@id='tweetUsername-%s']";
    private String tweetPostDateLocator = "//span[@id='tweetCreationDate-%s']";
    private String tweetContentLocator = "//p[@id='tweetDescription-%s']";

    private String userLocator = "//li[starts-with(@id,'user')]";
    private String userUsernameLocator = "//span[@id='userName-%s']";
    private String userCreationDateLocator = "//span[@id='userCreationDate-%s']";
    private String userDescriptionLocator = "//p[@id='userDescription-%s']";

    public List<TweetSearchModel> getAllSearchTweets(){
        logger().info("Getting all tweets");

        List<TweetSearchModel> tweetModelList = new ArrayList<>();

        tweetsSearchElements.forEach(webElement -> {
            TweetSearchModel tweetModel = new TweetSearchModel();

            WebElement tweet = webElement.findElement(By.xpath(tweetLocator));
            String tweetId = tweet.getAttribute("id").replace("tweet-","");

            tweetModel.setId(tweetId);
            tweetModel.setUsername(tweet.findElement(By.xpath(String.format(tweetUsernameLocator, tweetId))).getText());
            tweetModel.setPostDate(tweet.findElement(By.xpath(String.format(tweetPostDateLocator, tweetId))).getText());
            tweetModel.setContent(tweet.findElement(By.xpath(String.format(tweetContentLocator, tweetId))).getText());

            tweetModelList.add(tweetModel);
        });

        return tweetModelList;
    }

    public List<UserSearchModel> getAllSearchUsers(){
        logger().info("Getting all followers");
        List<UserSearchModel> userSearchModelList = new ArrayList<>();

        usersSearchElements.forEach(webElement -> {
            UserSearchModel userSearchModel = new UserSearchModel();

            WebElement user = webElement.findElement(By.xpath(userLocator));
            String userId = user.getAttribute("id").replace("user-","");

            userSearchModel.setId(userId);
            userSearchModel.setUsername(user.findElement(By.xpath(String.format(userUsernameLocator, userId))).getText());
            userSearchModel.setDescription(user.findElement(By.xpath(String.format(userDescriptionLocator, userId))).getText());
            userSearchModel.setCreationDate(user.findElement(By.xpath(String.format(userCreationDateLocator, userId))).getText());

            userSearchModelList.add(userSearchModel);
        });

        return userSearchModelList;
    }

    public String getSearchTweetsText(){
        return tweetSearchResult.getText();
    }

    public String getSearchUsersText(){
        return userSearchResult.getText();
    }

    public boolean isDisplayed(){
        try {
            searchHeader.getText();
            return true;
        } catch (NoSuchElementException e){
            return false;
        }
    }

}
