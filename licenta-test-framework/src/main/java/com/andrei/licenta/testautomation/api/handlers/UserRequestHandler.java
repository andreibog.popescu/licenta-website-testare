package com.andrei.licenta.testautomation.api.handlers;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.APIPaths;
import com.andrei.licenta.testautomation.model.AuthenticationModel;
import com.andrei.licenta.testautomation.model.GenericResponse;
import com.andrei.licenta.testautomation.model.UserModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;

public class UserRequestHandler extends RequestHandler {

    public UserRequestHandler(APIConnection apiConnection) {
        super(apiConnection);
    }

    public GenericResponse registerUser(UserModel user) throws JsonProcessingException {
        logger().info("Registering user {} using API", user);
        getApiConnection()
                .setBody(getJson(user), ContentType.JSON)
                .setBasePath(APIPaths.registerUser)
                .sendPost();

        return getApiConnectionResponse();
    }

    public GenericResponse editDescription(UserModel user) throws JsonProcessingException {
        logger().info("Editing description for user {} with '{}'", user, user.getDescription());
        getApiConnection()
                .setBody(getJson(user), ContentType.JSON)
                .setBasePath(String.format(APIPaths.userName, user.getUsername()))
                .sendPut();

        return getApiConnectionResponse();
    }

    public GenericResponse getUserDetails(UserModel user) {
        logger().info("Get details for user {}", user);
        getApiConnection()
                .setBasePath(String.format(APIPaths.userName, user.getUsername()))
                .sendGet();

        return getApiConnectionResponse();
    }

    public GenericResponse followUser(UserModel user) {
        logger().info("Following user {}", user.getUsername());
        getApiConnection()
                .setBasePath(String.format(APIPaths.followUser, user.getId()))
                .sendPost();

        return getApiConnectionResponse();
    }

    public GenericResponse unfollowUser(UserModel user) {
        logger().info("Unfollowing user {}", user.getUsername());
        getApiConnection()
                .setBasePath(String.format(APIPaths.unfollowUser, user.getId()))
                .sendPost();

        return getApiConnectionResponse();
    }

    public GenericResponse getFollowers(UserModel user) {
        logger().info("Get followers for user {}", user.getUsername());
        getApiConnection()
                .setBasePath(String.format(APIPaths.followers, user.getUsername()))
                .sendGet();

        return getApiConnectionResponse();
    }

    public GenericResponse cleanup(){
        logger().info("Begin cleanup");

        setAuthenticationModel(new AuthenticationModel("admin@licenta.fmi", "licenta"));

        getApiConnection()
                .setBasePath(APIPaths.cleanup)
                .sendDelete();

        return getApiConnectionResponse();
    }
}
