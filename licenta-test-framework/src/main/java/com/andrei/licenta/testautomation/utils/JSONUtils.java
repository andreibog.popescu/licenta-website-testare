package com.andrei.licenta.testautomation.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtils {
    private static ObjectMapper jacksonMapper = new ObjectMapper();

    public static ObjectMapper getJsonMapper() {
        return jacksonMapper;
    }
}
