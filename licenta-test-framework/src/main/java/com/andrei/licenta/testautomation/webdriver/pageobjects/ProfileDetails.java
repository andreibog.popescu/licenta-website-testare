package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class ProfileDetails extends PageObjectBase implements LogProvider {

    @FindBy(xpath = "//*[@id=\"profileName\"]")
    private WebElement profileUsername;

    @FindBy(xpath = "//*[@id=\"profileEmail\"]")
    private WebElement profileEmail;

    @FindBy(xpath = "//*[@id=\"profileDescription\"]")
    private WebElement profileDescription;

    @FindBy(xpath = "//*[@id=\"profileDate\"]")
    private WebElement profileJoinDate;

    @FindBy(xpath = "//*[@id=\"followButton\"]")
    private WebElement followButton;

    @FindBy(xpath = "//*[@id=\"followButton\"]/i")
    private WebElement followStatus;

    @FindBy(xpath = "//*[@id=\"textareaDesc\"]")
    private WebElement editProfileDescription;

    @FindBy(xpath = "//*[@id=\"editDescription\"]")
    private WebElement editProfileDescriptionButton;

    public ProfileDetails(WebDriver webDriver) {
        super(webDriver);
    }

    public String getProfileUsername() {
        return profileUsername.getText();
    }

    public String getProfileEmail() {
        return profileEmail.getText();
    }

    public String getProfileDescription() {
        return profileDescription.getText();
    }

    public String getProfileJoinDate() {
        return profileJoinDate.getText();
    }

    public void followUser() {
        logger().info("Following user {}", getProfileUsername());
        followButton.click();
    }

    public boolean isFollowed() {
        logger().info("Checking if user {} is followed", getProfileUsername());
        try {
            return followStatus.getText().equals("favorite");
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void editDescription(String text) {
        logger().info("Add description [{}] to user {}", text, getProfileUsername());
        editProfileDescription.clear();
        editProfileDescription.sendKeys(text);

        editProfileDescriptionButton.click();
    }

    public boolean canEditDescription() {
        logger().info("Checking if user can edit profile description for user {}", getProfileUsername());
        try {
            editProfileDescription.clear();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
