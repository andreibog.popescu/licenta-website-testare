package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.model.TweetModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class ProfileTweets extends PageObjectBase implements LogProvider {

    @FindBy(xpath = "//*[@id=\"textarea\"]")
    private WebElement newTweetInputEditor;
    @FindBy(xpath = "//*[@id=\"tweetButton\"]")
    private WebElement newTweetSubmitButton;

    @FindBy(xpath = "//*[@id=\"tweetsTitle\"]//span")
    private WebElement tweetsHeader;
    @FindBy(xpath = "//*[@id=\"noTweets\"]//span")
    private WebElement noTweetsLabel;
    @FindBy(xpath = "//ul[@id=\"tweets\"]/li")
    private List<WebElement> tweetElements;

    private String tweetLocator = "//form[starts-with(@id,'tweet')]";
    private String tweetUsernameLocator = "//span[@id='tweetUsername-%s']";
    private String tweetPostDateLocator = "//span[@id='tweetCreationDate-%s']";
    private String tweetContentLocator = "//p[@id='tweetDescription-%s']";
    private String tweetLikesLocator = "//span[@id='tweetLikeCount-%s']";
    private String tweetLikeButtonLocator = "//button[@id='tweetLikeButton-%s']";

    public ProfileTweets(WebDriver webDriver) {
        super(webDriver);
    }

    public void addNewTweet(String tweet){
        logger().info("Adding new tweet: {}", tweet);
        newTweetInputEditor.click();
        newTweetInputEditor.sendKeys(tweet);

        newTweetSubmitButton.click();
    }

    public List<TweetModel> getAllProfileTweets(){
        logger().info("Getting all tweets");

        List<TweetModel> tweetModelList = new ArrayList<>();

        tweetElements.forEach(webElement -> {
            TweetModel tweetModel = new TweetModel();

            WebElement tweetForm = webElement.findElement(By.xpath(tweetLocator));
            String tweetId = tweetForm.getAttribute("id").replace("tweet-","");

            tweetModel.setId(tweetId);
            tweetModel.setUsername(tweetForm.findElement(By.xpath(String.format(tweetUsernameLocator, tweetId))).getText());
            tweetModel.setPostDate(tweetForm.findElement(By.xpath(String.format(tweetPostDateLocator, tweetId))).getText());
            tweetModel.setContent(tweetForm.findElement(By.xpath(String.format(tweetContentLocator, tweetId))).getText());
            tweetModel.setLikeCount(Integer.parseInt(tweetForm.findElement(By.xpath(String.format(tweetLikesLocator, tweetId))).getText()));
            tweetModel.setLiked(isTweetLiked(tweetId));

            tweetModelList.add(tweetModel);
        });

        return tweetModelList;
    }

    public boolean noTweetsPresent(){
        try {
            return noTweetsLabel.getText().equalsIgnoreCase("Oops, you don't seem to have any tweets.") ||
                    noTweetsLabel.getText().equalsIgnoreCase("This user does not have any tweets currently.");
        } catch (NoSuchElementException e){
            return false;
        }
    }

    private void changeLikeStatusTweet(String id, boolean isLike){
        if (isLike) logger().info("Liking tweet with id {}", id);
        else logger().info("Unliking tweet with id {}", id);
        getWebDriver().findElement(By.xpath(String.format(tweetLikeButtonLocator, id))).click();
    }

    public void likeTweet(String id) {
        changeLikeStatusTweet(id, true);
    }

    public void unlikeTweet(String id) {
        changeLikeStatusTweet(id, false);
    }

    public boolean isTweetLiked(String id){
        return getWebDriver().findElement(By.xpath(String.format(tweetLikeButtonLocator + "//i", id))).getText().trim().equalsIgnoreCase("star");
    }

    public String getTweetCount(String id){
        return getWebDriver().findElement(By.xpath(String.format(tweetLikesLocator, id))).getText();
    }

}
