package com.andrei.licenta.testautomation.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface LogProvider {

    default Logger logger() {
        return LoggerFactory.getLogger(this.getClass());
    }

}